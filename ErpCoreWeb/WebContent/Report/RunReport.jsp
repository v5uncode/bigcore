<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ page import="com.ErpCoreWeb.Common.Global" %>
<%@ page import="com.ErpCoreModel.Report.CReport" %>
<%@ page import="com.ErpCoreModel.Report.CStatItem" %>
<%@ page import="com.ErpCoreModel.Framework.Util" %>
<%@ page import="com.ErpCoreModel.Base.CCompany" %>
<%@ page import="com.ErpCoreModel.Base.CUser" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObject" %>
<%@ page import="com.ErpCoreModel.Framework.CColumn" %>
<%@ page import="com.ErpCoreModel.Framework.CTable" %>
<%@ page import="java.util.UUID" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Collections" %>
<%@ page import="java.util.Comparator" %>
<%
if (request.getSession().getAttribute("User") == null)
{
    response.sendRedirect("../Login.jsp");
    return ;
}
%>
<%
CTable m_Table = null;
CCompany m_Company = null;
CReport m_Report = null;

if (request.getSession().getAttribute("User") == null)
{
	response.sendRedirect("../Login.jsp");
	response.getWriter().close();
	return ;
}

String RPT_Reprot_id = request.getParameter("id");
if (Global.IsNullParameter(RPT_Reprot_id))
{
	response.getWriter().close();
    return;
}

String B_Company_id = request.getParameter("B_Company_id");
if (Global.IsNullParameter(B_Company_id))
    m_Company = Global.GetCtx(this.getServletContext()).getCompanyMgr().FindTopCompany();
else
    m_Company = (CCompany)Global.GetCtx(this.getServletContext()).getCompanyMgr().Find(Util.GetUUID(B_Company_id));

m_Table = (CTable)m_Company.getReportMgr().getTable();

m_Report = (CReport)m_Company.getReportMgr().Find(Util.GetUUID(RPT_Reprot_id));

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
    <link href="../lib/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <script src="../lib/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/core/base.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerGrid.js" type="text/javascript"></script> 
    <script src="../lib/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerMenu.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerMenuBar.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerToolBar.js" type="text/javascript"></script>
    <script type="text/javascript">
        
    </script>
    <style type="text/css">
    #menu1,.l-menu-shadow{top:30px; left:50px;}
    #menu1{  width:200px;}
    </style>
    
    <script type="text/javascript">
        var grid;
        $(function ()
        {
            grid = $("#gridTable").ligerGrid({
            columns: [
                <%
                List<CStatItem> lstStatItem = new ArrayList<CStatItem>();
                List<CBaseObject> lstObj = m_Report.getStatItemMgr().GetList();
                for (CBaseObject obj : lstObj)
                {
                    lstStatItem.add((CStatItem)obj);
                }
                Collections.sort(lstStatItem, new Comparator() {
    				public int compare(Object a, Object b) {
    					return ((CStatItem) a).getIdx() - ((CStatItem) b).getIdx();
    				}
    			});
                
                for (int i=0;i<lstStatItem.size();i++)
                {
                    CStatItem item = (CStatItem)lstStatItem.get(i);
                    if(i<lstStatItem.size()-1)
                    	out.print(String.format("{ display: '%s', name: '%s'},",item.getName(),item.getName()));
                    else
                    	out.print(String.format("{ display: '%s', name: '%s'}",item.getName(),item.getName()));
                }
                 %>
                ],
                url: 'RunReport.jsp?Action=GetData&id=<%=request.getParameter("id") %>&B_Company_id=<%=request.getParameter("B_Company_id") %>',
                dataAction: 'server', 
                usePager: false,
                width: '100%', height: '100%',
                onSelectRow: function (data, rowindex, rowobj)
                {
                    //$.ligerDialog.alert('1选择的是' + data.id);
                }
            });
        });
        
    </script>
</head>
<body style="padding:6px; overflow:hidden;"> 
   <div id="gridTable" style="margin:0; padding:0"></div>

</body>
</html>