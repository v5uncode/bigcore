package com.ErpCoreWeb.Menu;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ErpCoreModel.Framework.CBaseObject;
import com.ErpCoreModel.Framework.CTable;
import com.ErpCoreModel.Framework.Util;
import com.ErpCoreModel.UI.CMenu;
import com.ErpCoreWeb.Common.Global;

/**
 * Servlet implementation class MenuPanel
 */
@WebServlet("/MenuPanel")
public class MenuPanel extends HttpServlet {
	private static final long serialVersionUID = 1L;

	HttpServletRequest request;
	HttpServletResponse response;

    public CTable m_Table = null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MenuPanel() {
        super();
        // TODO Auto-generated constructor stub
    }

    //初始化变量
    void initData()
    {
        if (request.getSession().getAttribute("User") == null)
        {
            try {
				response.sendRedirect("../Login.jsp");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return ;
        }

		m_Table = Global.GetCtx(this.getServletContext()).getMenuMgr()
				.getTable();

    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}
	void doProc(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		this.request=request;
		this.response=response;

		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");

		//初始化变量
	    initData();
	    //
        String Action = request.getParameter("Action");
        if (Action == null) Action = "";
        
        if (Action.equalsIgnoreCase("GetMenu"))
        {
        	GetMenu();
            return ;
        }
        else if (Action.equalsIgnoreCase("GetMenuName"))
        {
        	GetMenuName();
            return ;
        }
        else if (Action.equalsIgnoreCase("Delete"))
        {
        	Delete();
            return ;
        }

	}
    void GetMenu()
    {
		try {
			String pid = request.getParameter("pid");
			UUID Parent_id = Util.GetEmptyUUID();
			if (!Global.IsNullParameter(pid))
				Parent_id = Util.GetUUID(pid);
			String sJson = "[";
			List<CBaseObject> lstObj = Global.GetCtx(this.getServletContext())
					.getMenuMgr().GetList();
			for (CBaseObject obj : lstObj) {
				CMenu Menu = (CMenu) obj;
				if (Menu.getParent_id().equals(Parent_id)) {
					String sIconUrl = String.format(
							"../%s/MenuIcon/default.png",
							Global.GetDesktopIconPathName());
					if (Menu.getIconUrl().length() > 0) {
						sIconUrl = String.format("../%s/MenuIcon/%s",
								Global.GetDesktopIconPathName(),
								Menu.getIconUrl());
					}
					String sItem = String
							.format("{ isexpand: \"false\", \"id\":\"%s\",\"text\": \"%s\",\"icon\":\"%s\", children: []},",
									Menu.getId().toString(), Menu.getName(),
									sIconUrl);
					sJson += sItem;
				}
			}
			if (sJson.length() > 0 && sJson.endsWith(","))
				sJson = sJson.substring(0, sJson.length() - 1);
			sJson += "]";
			response.getWriter().print(sJson);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    void GetMenuName()
    {
		try {
			String id = request.getParameter("id");
			if (Global.IsNullParameter(id))
				return;
			CMenu menu = (CMenu) Global.GetCtx(this.getServletContext())
					.getMenuMgr().Find(Util.GetUUID(id));
			response.getWriter().print(menu.getName());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    void Delete()
    {
		try {
			String delid = request.getParameter("delid");
			if (Global.IsNullParameter(delid)) {
				response.getWriter().print("请选择记录！");
				return;
			}
			Global.GetCtx(this.getServletContext()).getMenuMgr()
					.Delete(Util.GetUUID(delid));
			if (!Global.GetCtx(this.getServletContext()).getMenuMgr()
					.Save(true)) {
				response.getWriter().print("删除失败！");
				return;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
