<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.ErpCoreWeb.Common.Global" %>
<%@ page import="com.ErpCoreModel.Framework.CTable" %>
<%@ page import="com.ErpCoreModel.Framework.Util" %>
<%@ page import="com.ErpCoreModel.Workflow.CWorkflow" %>
<%@ page import="com.ErpCoreModel.Workflow.enumApprovalState" %>
<%@ page import="com.ErpCoreModel.Base.CUser" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObjectMgr" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.UUID" %>

<%

if (request.getSession().getAttribute("User") == null)
{
    response.sendRedirect("../Login.jsp");
    return ;
}
CUser m_User = (CUser)request.getSession().getAttribute("User");

String TbCode = request.getParameter("TbCode");
String id = request.getParameter("id");
String WF_WorkflowDef_id = request.getParameter("WF_WorkflowDef_id");

if (Global.IsNullParameter(TbCode)
    || Global.IsNullParameter(id)
    || Global.IsNullParameter(WF_WorkflowDef_id))
{
    response.getWriter().print("数据不完整！");
    response.getWriter().close();
    return;
}

UUID guidParentId = Util.GetEmptyUUID();
String ParentId = request.getParameter("ParentId");
if (!Global.IsNullParameter(ParentId))
    guidParentId = Util.GetUUID(ParentId);

CBaseObjectMgr BaseObjectMgr = Global.GetCtx(this.getServletContext()).FindBaseObjectMgrCache(TbCode, guidParentId);
if (BaseObjectMgr == null)
{
    BaseObjectMgr = new CBaseObjectMgr();
    BaseObjectMgr.TbCode = TbCode;
    BaseObjectMgr.Ctx = Global.GetCtx(this.getServletContext());
}
//只能存在一个运行的工作流实例
UUID objId = Util.GetUUID(id);
List<CWorkflow> lstWF = BaseObjectMgr.getWorkflowMgr().FindLastByRowid(objId);
for (CWorkflow wf : lstWF)
{
    if (wf.getWF_WorkflowDef_id().equals(Util.GetUUID(WF_WorkflowDef_id))
        && wf.getState() == enumApprovalState.Running)
    {
    	response.getWriter().print("该工作流已经启动！");
        response.getWriter().close();
        return;
    }
}
//创建工作流实例并运行
CUser user = m_User;
CWorkflow Workflow = new CWorkflow();
Workflow.Ctx = Global.GetCtx(this.getServletContext());
Workflow.setWF_WorkflowDef_id (Util.GetUUID(WF_WorkflowDef_id));
Workflow.setState ( enumApprovalState.Init);
Workflow.setRow_id( objId);
Workflow.setCreator ( user.getId());
Workflow.setB_Company_id ( user.getB_Company_id());
StringBuffer sErr = new StringBuffer();
if (!BaseObjectMgr.getWorkflowMgr().StartWorkflow(Workflow, sErr))
{
	response.getWriter().print(String.format("启动工作流失败：%s", sErr.toString()));
    response.getWriter().close();
    return;
}
BaseObjectMgr.getWorkflowMgr().AddNew(Workflow);
if (!BaseObjectMgr.getWorkflowMgr().Save(true))
{
	response.getWriter().print("创建工作流失败！");
    response.getWriter().close();
    return;
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head >
    <title></title>
</head>
<body>
    <form id="form1" >
    <div>
    
    </div>
    </form>
</body>
</html>