package com.ErpCoreWeb.Window;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.dispatcher.multipart.MultiPartRequestWrapper;

import com.ErpCoreModel.Base.CUser;
import com.ErpCoreModel.Framework.CBaseObject;
import com.ErpCoreModel.Framework.CBaseObjectMgr;
import com.ErpCoreModel.Framework.CColumn;
import com.ErpCoreModel.Framework.CTable;
import com.ErpCoreModel.Framework.ColumnType;
import com.ErpCoreModel.Framework.DatabaseType;
import com.ErpCoreModel.Framework.Util;
import com.ErpCoreWeb.Common.EditObject;
import com.ErpCoreWeb.Common.Global;
import com.mongodb.BasicDBObject;

/**
 * Servlet implementation class Edit
 */
@WebServlet("/Window_Edit")
public class Edit extends HttpServlet {
	private static final long serialVersionUID = 1L;

	HttpServletRequest request;
	HttpServletResponse response;
    MultiPartRequestWrapper wrapper ;

	public CUser m_User = null;
    public CTable m_Table = null;
    public UUID m_guidParentId = Util.GetEmptyUUID();
    public CBaseObjectMgr m_BaseObjectMgr = null;
    public CBaseObject m_BaseObject = null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Edit() {
        super();
        // TODO Auto-generated constructor stub
    }

    //初始化变量
    void initData()
    {
        if (request.getSession().getAttribute("User") == null)
        {
            try {
				response.sendRedirect("../Login.jsp");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return ;
        }
        m_User=(CUser)request.getSession().getAttribute("User");
        if (!m_User.IsRole("管理员"))
        {
        	try {
				response.getWriter().print("没有管理员权限！");
	        	response.getWriter().close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	return ;
        }
		try {
	        if(IsMultipart())
	        	wrapper = (MultiPartRequestWrapper) request;

			String tid = GetParameter("tid");
			if (Global.IsNullParameter(tid)) {
				response.getWriter().close();
				return ;
			}
			m_Table = (CTable) Global.GetCtx(this.getServletContext())
					.getTableMgr().Find(Util.GetUUID(tid));

			String ParentId = GetParameter("ParentId");
			if (!Global.IsNullParameter(ParentId))
				m_guidParentId = Util.GetUUID(ParentId);

			String id = GetParameter("id");
			if (Global.IsNullParameter(id)) {
				response.getWriter().print("请选择记录！");
				response.getWriter().close();
			}
			m_BaseObjectMgr = Global.GetCtx(this.getServletContext())
					.FindBaseObjectMgrCache(m_Table.getCode(), m_guidParentId);
			if (m_BaseObjectMgr == null) {
				m_BaseObjectMgr = new CBaseObjectMgr();
				m_BaseObjectMgr.TbCode = m_Table.getCode();
				m_BaseObjectMgr.Ctx = Global.GetCtx(this.getServletContext());
				if (m_BaseObjectMgr.Ctx.getMainDB().m_DbType == DatabaseType.MongoDb)
                {
                	BasicDBObject query = new BasicDBObject();  
                    query.put("_id", id);
                    m_BaseObjectMgr.GetList(query, null, 0, 0);
                }
				else
				{
					String sWhere = String.format(" id='%s'", id);
					m_BaseObjectMgr.GetList(sWhere);
				}
			}
			m_BaseObject = m_BaseObjectMgr.Find(Util.GetUUID(id));
			if (m_BaseObject == null) {
				response.getWriter().print("请选择记录！");
				response.getWriter().close();
			}

			// 保存到编辑对象
			EditObject.Add(request.getSession().getId(), m_BaseObject);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    boolean IsMultipart()
    {
    	String ct=request.getContentType();
    	if(Global.IsNullParameter(ct))
    		return false;
    	if(ct.indexOf("multipart/form-data")>-1)
    		return true;
    	return false;
    }
    String GetParameter(String key)
    {
    	if(IsMultipart())
    		return wrapper.getParameter(key);
    	else
    		return request.getParameter(key);
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doProc(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}
	void doProc(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		this.request=request;
		this.response=response;

		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");

		//初始化变量
	    initData();
	    //
        String Action = wrapper.getParameter("Action");
        if (Action == null) Action = "";

        if (Action.equalsIgnoreCase("Cancel"))
        {
            m_BaseObject.Cancel();
        	response.getWriter().close();
            return ;
        }
        else if (Action.equalsIgnoreCase("PostData"))
        {
        	PostData();
            //从编辑对象移除
            EditObject.Remove(request.getSession().getId(), m_BaseObject);
        	response.getWriter().close();
            return ;
        }
	}

    void PostData()
    {
		try {
			if (!ValidateData())
				return;

			List<CBaseObject> lstCol = m_Table.getColumnMgr().GetList();
			boolean bHasVisible = false;
			for (CBaseObject obj : lstCol) {
				CColumn col = (CColumn) obj;

				if (col.getCode().equalsIgnoreCase("id"))
					continue;
				else if (col.getCode().equalsIgnoreCase("Created")) {

					continue;
				} else if (col.getCode().equalsIgnoreCase("Creator")) {
					continue;
				} else if (col.getCode().equalsIgnoreCase("Updated")) {
					m_BaseObject.SetColValue(col,
							new Date(System.currentTimeMillis()));
					continue;
				} else if (col.getCode().equalsIgnoreCase("Updator")) {
					CUser user = (CUser) request.getSession().getAttribute(
							"User");
					m_BaseObject.SetColValue(col, user.getId());
					continue;
				}

				if (col.getColType() == ColumnType.object_type) {
					File postfile = wrapper.getFiles("_" + col.getCode())[0];
					if (postfile != null && postfile.length() > 0) {
						String sFileName = wrapper.getFileNames("_"
								+ col.getCode())[0];
						if (sFileName.lastIndexOf('\\') > -1)// 有些浏览器不带路径
							sFileName = sFileName.substring(sFileName
									.lastIndexOf('\\'));

						byte[] byteFileName = sFileName.getBytes("UTF8");
						byte[] byteValue = new byte[(int) (254 + postfile
								.length())];
						byte[] byteData = new byte[(int) postfile.length()];
						InputStream in = new FileInputStream(postfile);
						in.read(byteData);

						System.arraycopy(byteFileName, 0, byteValue, 0,
								byteFileName.length);
						System.arraycopy(byteData, 0, byteValue, 254,
								byteData.length);

						m_BaseObject.SetColValue(col, byteValue);
					}
				} else if (col.getColType() == ColumnType.path_type) {
					String sUploadPath = col.getUploadPath();
					if (!sUploadPath.endsWith("\\"))
						sUploadPath += "\\";
					File dir = new File(sUploadPath);
					if (!dir.exists())
						dir.mkdir();

					File postfile = wrapper.getFiles("_" + col.getCode())[0];
					if (postfile != null && postfile.length() > 0) {
						String sFileName = wrapper.getFileNames("_"
								+ col.getCode())[0];
						if (sFileName.lastIndexOf('\\') > -1)// 有些浏览器不带路径
							sFileName = sFileName.substring(sFileName
									.lastIndexOf('\\'));

						String sExt = "";
						int idx = sFileName.lastIndexOf('.');
						if (idx > -1)
							sExt = sFileName.substring(idx);
						// File fi = new File(sUploadPath + sFileName);
						UUID guid = UUID.randomUUID();
						String sDestFile = String.format("%s%s", guid
								.toString().replace("-", ""), sExt);

						byte[] buffer = new byte[1024];
						FileOutputStream fos = new FileOutputStream(sUploadPath
								+ sDestFile);
						InputStream in = new FileInputStream(postfile);
						try {
							int num = 0;
							while ((num = in.read(buffer)) > 0) {
								fos.write(buffer, 0, num);
							}
						} catch (Exception e) {
							e.printStackTrace(System.err);
						} finally {
							in.close();
							fos.close();
						}

						String sVal = String.format("%s%s", sDestFile,
								sFileName);
						m_BaseObject.SetColValue(col, sVal);
					}
				} else if (col.getColType() == ColumnType.bool_type) {
					String val = wrapper.getParameter("_" + col.getCode());
					if (!Global.IsNullParameter(val)
							&& val.equalsIgnoreCase("on"))
						m_BaseObject.SetColValue(col, true);
					else
						m_BaseObject.SetColValue(col, false);
				} else if (col.getColType() == ColumnType.datetime_type) {
					String val = wrapper.getParameter("_" + col.getCode());
					if (!Global.IsNullParameter(val)) {
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
						if(val.indexOf(" ")>0)
							sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						Date V;
						try {
							V = sdf.parse(val);
							m_BaseObject.SetColValue(col, V);
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				} else
					m_BaseObject.SetColValue(col,
							wrapper.getParameter("_" + col.getCode()));
				bHasVisible = true;
			}
			if (!bHasVisible) {
				// Response.Write("没有可修改字段！");
				response.getWriter()
						.print("<script>parent.$.ligerDialog.warn('没有可修改字段！');</script>");
				return;
			}
			if (!m_BaseObjectMgr.Update(m_BaseObject, true)) {
				// Response.Write("修改失败！");
				response.getWriter().print(
						"<script>parent.$.ligerDialog.warn('修改失败！');</script>");
				return;
			}
			// 在iframe里访问外面,需要parent.parent.
			// Response.Write("<script>parent.parent.grid.loadData(true);parent.parent.$.ligerDialog.close();</script>");
			response.getWriter().print(
					"<script>parent.parent.onOkEdit();</script>");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    //验证数据
    boolean ValidateData()
    {
		try {
			List<CBaseObject> lstCol = m_Table.getColumnMgr().GetList();
			for (CBaseObject obj : lstCol) {
				CColumn col = (CColumn) obj;

				if (col.getCode().equalsIgnoreCase("id"))
					continue;
				else if (col.getCode().equalsIgnoreCase("Created"))
					continue;
				else if (col.getCode().equalsIgnoreCase("Creator"))
					continue;
				else if (col.getCode().equalsIgnoreCase("Updated"))
					continue;
				else if (col.getCode().equalsIgnoreCase("Updator"))
					continue;

				String val = wrapper.getParameter("_" + col.getCode());
				if (!col.getAllowNull() && Global.IsNullParameter(val)) {
					response.getWriter()
							.print(String
									.format("<script>parent.$.ligerDialog.warn('%s不允许空！');</script>",
											col.getName()));
					return false;
				}

				if (col.getColType() == ColumnType.string_type) {
					if (val.length() > col.getColLen()) {
						response.getWriter()
								.print(String
										.format("<script>parent.$.ligerDialog.warn('%s长度不能超过%d！');</script>",
												col.getName(), col.getColLen()));
						return false;
					}
				} else if (col.getColType() == ColumnType.datetime_type) {
					if (!Global.IsNullParameter(val)) {
						try {
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
							if(val.indexOf(" ")>0)
								sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							Date V2 = sdf.parse(val);
						} catch (Exception e) {
							response.getWriter()
									.print(String
											.format("<script>parent.$.ligerDialog.warn('%s日期格式错误！');</script>",
													col.getName()));
							return false;
						}
					}
				} else if (col.getColType() == ColumnType.int_type
						|| col.getColType() == ColumnType.long_type) {
					if (!Util.IsInt(val)) {
						response.getWriter()
								.print(String
										.format("<script>parent.$.ligerDialog.warn('%s为整型数字！');</script>",
												col.getName()));
						return false;
					}
				} else if (col.getColType() == ColumnType.numeric_type) {
					if (!Util.IsNum(val)) {
						response.getWriter()
								.print(String
										.format("<script>parent.$.ligerDialog.warn('%s为数字！');</script>",
												col.getName()));
						return false;
					}
				} else if (col.getColType() == ColumnType.guid_type
						|| col.getColType() == ColumnType.ref_type) {
					if (!Global.IsNullParameter(val)) {
						try {
							Util.GetUUID(val);
						} catch (Exception e) {
							response.getWriter()
									.print(String
											.format("<script>parent.$.ligerDialog.warn('%s为GUID格式！');</script>",
													col.getName()));
							return false;
						}
					}
				}

			}
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
    }
}
