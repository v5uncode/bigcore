﻿// File:    CPriceNote.java
// Author:  甘孝俭
// email:   ganxiaojian@hotmail.com 
// QQ:      154986287
// http://www.8088net.com
// 协议声明：本软件为开源系统，遵循国际开源组织协议。任何单位或个人可以使用或修改本软件源码，
//          可以用于作为非商业或商业用途，但由于使用本源码所引起的一切后果与作者无关。
//          未经作者许可，禁止任何企业或个人直接出售本源码或者把本软件作为独立的功能进行销售活动，
//          作者将保留追究责任的权利。
// Created: 2015/12/6 10:23:34
// Purpose: Definition of Class CPriceNote

package com.ErpCoreModel.Store;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import com.ErpCoreModel.Framework.CBaseObject;
import com.ErpCoreModel.Framework.Util;
import com.ErpCoreModel.Framework.CValue;

public class CPriceNote extends CBaseObject
{

    public CPriceNote()
    {
        TbCode = "XS_PriceNote";
        ClassName = "com.ErpCoreModel.Store.CPriceNote";

    }

    
        public String getCode()
        {
            if (m_arrNewVal.containsKey("code"))
                return m_arrNewVal.get("code").StrVal;
            else
                return "";
        }
        public void setCode(String value)
        {
            if (m_arrNewVal.containsKey("code"))
                m_arrNewVal.get("code").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("code", val);
            } 
       }
        public String getTo()
        {
            if (m_arrNewVal.containsKey("to"))
                return m_arrNewVal.get("to").StrVal;
            else
                return "";
        }
        public void setTo(String value)
        {
            if (m_arrNewVal.containsKey("to"))
                m_arrNewVal.get("to").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("to", val);
            } 
       }
        public String getToContacts()
        {
            if (m_arrNewVal.containsKey("tocontacts"))
                return m_arrNewVal.get("tocontacts").StrVal;
            else
                return "";
        }
        public void setToContacts(String value)
        {
            if (m_arrNewVal.containsKey("tocontacts"))
                m_arrNewVal.get("tocontacts").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("tocontacts", val);
            } 
       }
        public String getToTel()
        {
            if (m_arrNewVal.containsKey("totel"))
                return m_arrNewVal.get("totel").StrVal;
            else
                return "";
        }
        public void setToTel(String value)
        {
            if (m_arrNewVal.containsKey("totel"))
                m_arrNewVal.get("totel").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("totel", val);
            } 
       }
        public String getToFax()
        {
            if (m_arrNewVal.containsKey("tofax"))
                return m_arrNewVal.get("tofax").StrVal;
            else
                return "";
        }
        public void setToFax(String value)
        {
            if (m_arrNewVal.containsKey("tofax"))
                m_arrNewVal.get("tofax").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("tofax", val);
            } 
       }
        public String getFrom()
        {
            if (m_arrNewVal.containsKey("from"))
                return m_arrNewVal.get("from").StrVal;
            else
                return "";
        }
        public void setFrom(String value)
        {
            if (m_arrNewVal.containsKey("from"))
                m_arrNewVal.get("from").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("from", val);
            } 
       }
        public String getFromContacts()
        {
            if (m_arrNewVal.containsKey("fromcontacts"))
                return m_arrNewVal.get("fromcontacts").StrVal;
            else
                return "";
        }
        public void setFromContacts(String value)
        {
            if (m_arrNewVal.containsKey("fromcontacts"))
                m_arrNewVal.get("fromcontacts").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("fromcontacts", val);
            } 
       }
        public String getFromTel()
        {
            if (m_arrNewVal.containsKey("fromtel"))
                return m_arrNewVal.get("fromtel").StrVal;
            else
                return "";
        }
        public void setFromTel(String value)
        {
            if (m_arrNewVal.containsKey("fromtel"))
                m_arrNewVal.get("fromtel").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("fromtel", val);
            } 
       }
        public String getFromFax()
        {
            if (m_arrNewVal.containsKey("fromfax"))
                return m_arrNewVal.get("fromfax").StrVal;
            else
                return "";
        }
        public void setFromFax(String value)
        {
            if (m_arrNewVal.containsKey("fromfax"))
                m_arrNewVal.get("fromfax").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("fromfax", val);
            } 
       }
        public UUID getB_Company_id()
        {
            if (m_arrNewVal.containsKey("b_company_id"))
                return m_arrNewVal.get("b_company_id").GuidVal;
            else
                return Util.GetEmptyUUID();
        }
        public void setB_Company_id(UUID value)
        {
            if (m_arrNewVal.containsKey("b_company_id"))
                m_arrNewVal.get("b_company_id").GuidVal = value;
            else
            {
                CValue val = new CValue();
                val.GuidVal = value;
                m_arrNewVal.put("b_company_id", val);
            }
        }
        public String getAttn()
        {
            if (m_arrNewVal.containsKey("attn"))
                return m_arrNewVal.get("attn").StrVal;
            else
                return "";
        }
        public void setAttn(String value)
        {
            if (m_arrNewVal.containsKey("attn"))
                m_arrNewVal.get("attn").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("attn", val);
            } 
       }
        public String getRemarks()
        {
            if (m_arrNewVal.containsKey("remarks"))
                return m_arrNewVal.get("remarks").StrVal;
            else
                return "";
        }
        public void setRemarks(String value)
        {
            if (m_arrNewVal.containsKey("remarks"))
                m_arrNewVal.get("remarks").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("remarks", val);
            } 
       }
}
